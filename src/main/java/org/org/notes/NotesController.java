package org.org.notes;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class NotesController {
    @FXML
    private Button addButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Label descriptionLabel;

    @FXML
    private TextArea descriptionNoteTextArea;

    @FXML
    private Label noteLabel;

    @FXML
    private ListView<String> noteList;

    @FXML
    private TextField noteTitleTextField;

    @FXML
    private Button saveButton;

    @FXML
    private Label titleLabel;

    @FXML
    void DeleteNote(ActionEvent event) {
        String selectedNote = noteList.getSelectionModel().getSelectedItem();
        if (selectedNote != null) {
            noteList.getItems().remove(selectedNote);
            clearFields();
        }
    }

    @FXML
    void NewNote(ActionEvent event) {
        if(event.getSource()==addButton){
            String defaultTitle = "New Note ";
            noteList.getItems().add(defaultTitle + ": ");
            noteList.getSelectionModel().selectLast();
            noteTitleTextField.setText(defaultTitle);
            descriptionNoteTextArea.clear();
        }
    }

    @FXML
    void SaveNote(ActionEvent event) {
        String newTitle = noteTitleTextField.getText();
        String newDescription = descriptionNoteTextArea.getText();
        if (!newTitle.isEmpty() && !newDescription.isEmpty()) {
            int selectedIndex = noteList.getSelectionModel().getSelectedIndex();
            if (selectedIndex != -1) {
                noteList.getItems().set(selectedIndex, newTitle + ": " + newDescription);
            }
        }
    }
    @FXML
    void NoteListClicked(MouseEvent event) {
        String selectedNote = noteList.getSelectionModel().getSelectedItem();
        if (selectedNote != null) {
            String[] parts = selectedNote.split(": ", 2);
            noteTitleTextField.setText(parts[0]);
            descriptionNoteTextArea.setText(parts[1]);
        }
    }
    private void clearFields() {
        noteTitleTextField.clear();
        descriptionNoteTextArea.clear();
    }
}
