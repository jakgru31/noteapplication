module org.org.notes {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;

    opens org.org.notes to javafx.fxml;
    exports org.org.notes;
}